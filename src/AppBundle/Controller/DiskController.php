<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Form;
use AppBundle\Form\Type\FormType;
use AppBundle\Form\Type\SearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DiskController extends Controller
{
    /**
     * @Route("/", name="list")
     */
    public function listAction(Request $request)
    {

        $Repo = $this->getDoctrine()->getRepository('AppBundle:Form');
        $rows = $Repo->findAll();

        return $this->render('disk/list.html.twig', array(
            'rows' => $rows
        ));
    }

    /**
     * @Route("/add", name="add")
     */
    public function addAction(Request $request)
    {
        $Form = new Form();

        $form = $this->createForm(FormType::class, $Form);

        $form->handleRequest($request);
        if($request->isMethod('POST')){
            if($form->isValid()){
                $entity_manager = $this->getDoctrine()->getManager();
                $entity_manager->persist($Form);
                $entity_manager->flush();

                $form->getData();
                return $this->redirect($this->generateUrl('list'));
            }else{
                //i cant create this album
            }}

        return $this->render('disk/add.html.twig', array(
            'form' => isset($form) ? $form->createView() : NULL
        ));
    }

    /**
     * @Route("/more/{id}", name="more")
     */
    public function moreAction(Request $request, $id)
    {
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Form');
        $rows = $Repo->find($id);

        if(NULL == $rows){
            throw $this->createNotFoundException('Not Found entry in this database');
        }

        return $this->render('disk/more.html.twig', array(
            'rows' => $rows
        ));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Form');
        $Form = $Repo->find($id);

        if (NULL == $Form) {
            throw $this->createNotFoundException('Not Found entry in this database');
        }

        $entity_manager = $this->getDoctrine()->getManager();
        $entity_manager->remove($Form);
        $entity_manager->flush();

        return $this->redirect($this->generateUrl('list'));
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction(Request $request, $id)
    {
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Form');
        $Form = $Repo->find($id);

        if(NULL == $Form){
            throw $this->createNotFoundException('Not Found entry in this database');
        }

        $form = $this->createForm(FormType::class, $Form);

        $form->handleRequest($request);
        if($request->isMethod('POST')){
            if($form->isValid()){
                $entity_manager = $this->getDoctrine()->getManager();
                $entity_manager->persist($Form);
                $entity_manager->flush();

                $form->getData();
                return $this->redirect($this->generateUrl('list'));
            }else{
                //i cant edited this entry
            }
        }

        return $this->render('disk/edit.html.twig', array(
            'rows' => $Form,
            'form' => isset($form) ? $form->createView() : NULL
        ));
    }


//    public function searchAction(Request $request)
//    {
//        $form = $this->createFormBuilder(null)
//            ->add('search', Type::class)
//            ->getForm();
//
//
//        return $this->render('disk/search.html.twig', [
//            'form' => $form->createView()
//        ]);
//    }


    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request){
//        $em = $this->getDoctrine()->getManager();
//        $Repo = $em->getRepository('AppBundle:Form');

        $search = $request->get('search');

//        $result = $this->getDoctrine()->getManager()->getRepository('AppBundle:Form')->findBy(array('album' => $search));
        $result = $this->getDoctrine()->getManager()->getRepository('AppBundle:Form')->createQueryBuilder('a')
            ->where('a.album = :search')
            ->orWhere('a.performer = :search')
            ->orWhere('a.releasedata = :search')
            ->orWhere('a.type = :search')
            ->orWhere('a.studio = :search')
            ->setParameter('search', $search)
            ->getQuery()
            ->getResult();

//        $Repo = $this->getDoctrine()->getRepository('AppBundle:Form');
//        $rows = $Repo->findBy(array(
//            'album' => ''
//        ));
        return $this->render('disk/listSearch.html.twig', [
            'rows' => $result
        ]);
//        if($request->getMethod()=='POST'){
//            $album = $request->get('album');
//
//            $search = $Repo->findOneBy(array('album' => $album));
//
//            if($search){
//                return $this->render('disk/listSearch.html.twig', array(
//                    'rows' => $search
//                ));
//            }else{
//                return $this->render('disk/list.html.twig');
//            }
//        }
    }

}
